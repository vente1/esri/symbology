ARG NGINX_IMAGE_TAG=1.19.1-alpine

FROM nginx:${NGINX_IMAGE_TAG}

LABEL maintainer="ilorgar@gesplan.es"

COPY rootfs /

EXPOSE 80

HEALTHCHECK --interval=30s --timeout=15s --start-period=1m --retries=10 \
	CMD wget --spider -q http://localhost/nginx-health \
		|| exit 1
